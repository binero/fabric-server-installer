#!/usr/bin/python3

from dataclasses import dataclass
import hashlib
import json
from pathlib import Path
import random
import string
import subprocess
from sys import stderr
from typing import Callable, TypeVar
from urllib3 import request, PoolManager

USER_AGENT = "Alinea Fabric-Installer"

T = TypeVar('T')


def find(list_to_search: list[T], matcher: Callable[[T], bool]) -> T | None:
    for item in list_to_search:
        if matcher(item):
            return item
    return None


def download_json(http: request.RequestMethods, url: str) -> dict:
    with http.request('GET', url, preload_content=False) as resp:
        return json.loads(resp.read().decode('utf-8'))


def download_file(http: request.RequestMethods, url: str, target_path: Path):
    # Write the file straight to disk to avoid loading the entire file into memory
    with target_path.open('wb') as target_file:
        with http.request('GET', url, preload_content=False) as resp:
            while True:
                data = resp.read(1024 * 1024)
                if not data:
                    break
                target_file.write(data)


@dataclass
class FabricInstallerVersion:
    url: str
    version: str
    maven: str

    def get_from_api(http: request.RequestMethods, version: str | None = None) -> 'FabricInstallerVersion':
        ENDPOINT = 'https://meta.fabricmc.net/v2/versions/installer'
        fabric_versions = download_json(http, ENDPOINT)

        if version is None:
            version_data = fabric_versions[0]
        else:
            version_data = find(fabric_versions, lambda v: v['version'] == version)

        if version_data is None:
            raise ValueError(f'Could not find Fabric Installer version {version}')

        return FabricInstallerVersion(
            url=version_data['url'],
            version=version_data['version'],
            maven=version_data['maven']
        )

    def download(self, http: request.RequestMethods, download_dir: Path) -> Path:
        installer_path = download_dir / f'fabric-installer-{self.version}.jar'

        if installer_path.exists():
            print(f'Installer already exists at {installer_path}', file=stderr)
        else:
            print(f'Downloading Fabric installer to {installer_path}', file=stderr)
            download_file(http, self.url, installer_path)
            print(f'Downloaded Fabric installer to {installer_path}', file=stderr)
        return installer_path


@dataclass
class MinecraftVersion:
    version: str
    stable: bool

    def get_from_api(http: request.RequestMethods, version: str | None = None) -> 'MinecraftVersion':
        ENDPOINT = 'https://meta.fabricmc.net/v2/versions/game'
        minecraft_versions = download_json(http, ENDPOINT)

        if version is None:
            version_data = minecraft_versions[0]
        else:
            version_data = find(minecraft_versions, lambda v: v['version'] == version)

        if version_data is None:
            raise ValueError(f'Could not find Minecraft version {version}')

        return MinecraftVersion(
            version=version_data['version'],
            stable=version_data['stable']
        )


@dataclass
class FabricLoaderVersion:
    separator: str
    build: int
    maven: str
    version: str
    stable: bool

    def get_from_api(http: request.RequestMethods, version: str | None = None) -> 'FabricLoaderVersion':
        ENDPOINT = 'https://meta.fabricmc.net/v2/versions/loader'
        fabric_loader_versions = download_json(http, ENDPOINT)

        if version is None:
            version_data = fabric_loader_versions[0]
        else:
            version_data = find(fabric_loader_versions, lambda v: v['version'] == version)

        if version_data is None:
            raise ValueError(f'Could not find Fabric Loader version {version}')

        return FabricLoaderVersion(
            separator=version_data['separator'],
            build=version_data['build'],
            maven=version_data['maven'],
            version=version_data['version'],
            stable=version_data['stable']
        )


@dataclass
class FabricApiVersion:
    id: str
    project_id: str
    author_id: str
    featured: bool
    name: str
    version_number: str
    changelog: str
    date_published: str
    downloads: int
    version_type: str
    file_sha512: str
    file_url: str
    file_name: str
    file_size: int

    def get_from_api(http: request.RequestMethods, minecraft_version: MinecraftVersion, version: str | None = None) -> 'FabricApiVersion':
        ENDPOINT = f'https://api.modrinth.com/v2/project/fabric-api/version?game_versions=["{minecraft_version.version}"]&loaders=["fabric"]'
        fabric_api_versions = download_json(http, ENDPOINT)

        if version is None:
            version_data = fabric_api_versions[0]
        else:
            version_data = find(fabric_api_versions, lambda v: v['version'] == version)

        if version_data is None:
            raise ValueError(f'Could not find Fabric API version {version}')

        file_data = find(version_data['files'], lambda f: f['primary'] == True)

        return FabricApiVersion(
            id=version_data['id'],
            project_id=version_data['project_id'],
            author_id=version_data['author_id'],
            featured=version_data['featured'],
            name=version_data['name'],
            version_number=version_data['version_number'],
            changelog=version_data['changelog'],
            date_published=version_data['date_published'],
            downloads=version_data['downloads'],
            version_type=version_data['version_type'],
            file_sha512=file_data['hashes']['sha512'],
            file_url=file_data['url'],
            file_name=file_data['filename'],
            file_size=file_data['size']
        )

    def download(self, http: request.RequestMethods, mods_directory: Path) -> Path:
        file_path = mods_directory / self.file_name

        print(f'Downloading Fabric API to {file_path}', file=stderr)
        download_file(http, self.file_url, file_path)

        with open(file_path, 'rb') as f:
            file_hash = hashlib.sha512(f.read()).hexdigest()
            if file_hash != self.file_sha512:
                raise ValueError(f'File hash does not match expected hash: {file_hash} != {self.file_sha512}')

        print(f'Downloaded Fabric API to {file_path}', file=stderr)
        return file_path


def install_fabric(installer_path: Path, download_directory: Path, minecraft_version: MinecraftVersion):
    # java -jar fabric-installer.jar server -dir OUTPUT_DIR -downloadMinecraft -mcversion MC_VERSION

    print(f'Installing Fabric to {download_directory}', file=stderr)
    subprocess.run([
        'java',
        '-jar',
        installer_path,
        'server',
        '-dir',
        download_directory,
        '-downloadMinecraft',
        '-mcversion',
        minecraft_version.version
    ], stderr=stderr, stdout=stderr)

    print(f'Installed Fabric to {download_directory}', file=stderr)


def create_download_directory(loader_version: FabricLoaderVersion, minecraft_version: MinecraftVersion, base_path: Path) -> Path:
    random_suffix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
    filename = f'{minecraft_version.version}-{loader_version.version}-{random_suffix}'

    download_path = base_path / filename
    download_path.mkdir(parents=True, exist_ok=True)

    return download_path


def accept_eula(download_path: Path):
    eula_path = download_path / 'eula.txt'
    eula_path.write_text('eula=true')


def main():
    DOWNLOAD_BASE_PATH = Path("fabric-servers")
    INSTALLERS_PATH = Path("fabric-installers")

    http = PoolManager(headers={'User-Agent': 'Alinea Fabric Installer'})

    installer_version = FabricInstallerVersion.get_from_api(http)
    minecraft_version = MinecraftVersion.get_from_api(http)
    loader_version = FabricLoaderVersion.get_from_api(http)
    api_version = FabricApiVersion.get_from_api(http, minecraft_version)

    INSTALLERS_PATH.mkdir(parents=True, exist_ok=True)
    installer_path = installer_version.download(http, INSTALLERS_PATH)

    download_path = create_download_directory(loader_version, minecraft_version, DOWNLOAD_BASE_PATH)
    install_fabric(installer_path, download_path, minecraft_version)
    accept_eula(download_path)

    mods_path = download_path / 'mods'
    mods_path.mkdir(parents=True, exist_ok=True)
    api_version.download(http, mods_path)

    print(f'Fabric server installed to {download_path}', file=stderr)


if __name__ == '__main__':
    main()
